class AddAvatarColumnsToPhoneNumber < ActiveRecord::Migration
  def up
    add_attachment :phone_numbers, :avatar
  end

  def down
    remove_attachment :phone_numbers, :avatar
  end
end
