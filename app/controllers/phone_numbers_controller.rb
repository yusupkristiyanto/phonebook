class PhoneNumbersController < ApplicationController
  before_action :authenticate_user!
  before_action :define_phone_number, only: [:show, :destroy, :edit, :update]

  def index
    @phone_numbers = PhoneNumber.accessible_by(current_ability)
  end

  def new
    @phone_number = PhoneNumber.new
    respond_to do |format|
      format.js
      format.html
    end
  end

  def create
    @phone_number = current_user.phone_numbers.new(phone_number_params)
    
    if @phone_number.save
      flash[:notice] = "New PhoneNumber succesfully added"
    end
    redirect_to '/phone_numbers'
  end

  def show
    respond_to do |format|
      format.js
      format.html
    end
  end

  def edit
  end

  def update
  	@phone_number.update(phone_number_params)
    redirect_to '/phone_numbers'
  end

  def destroy
    @phone_number.destroy
    redirect_to '/phone_numbers'
  end

  private

  def define_phone_number
    @phone_number = PhoneNumber.find(params[:id])
  end

  def phone_number_params
    params.require(:phone_number).permit(:name, :address, :companny, :number, :avatar)
  end
end
